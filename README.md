# Dashboard Backend

##Prerequisites
1. Java 9+ (9.0.4)
2. Docker 18.03.x (18.03.0-ce)
3. Docker-compose 1.20.x (1.20.1)
4. Maven 3+ (3.3.9)

## Setup
###Database
1. Install docker.
2. Give your docker file a real volume to store data, then run it
3. Connect to your database `mysql -p 3306`
4. Create the default database we are connecting to
```sql
CREATE DATABASE dashboard;
```
#####You're ready! 
Once application starts it will create tables and populate it with data (see /src/main/resources/db/migration/)

#####Run application:
```
mvn spring-boot:run 
```
API will be available under "/dashboard" context path (e.g.: "/dashboard/client/1")

####Things to improve
1. Finalize fund API
2. Add tests across all APIs
3. Security, maybe? 


## Task details

Consider an application a simpler version of our application.
We have those entities:
- Client - is a company with a name and a description
- Client can have one or more Investors that they represent - Investor manages one or more funds
We would want couple of REST end points: - get list of clients
- get list of investors for a client,
- get list of funds for an investor
- Update or create a client

###UI
Have a simple dashboard with links that would allow:
- display list of clients
- When client is clicked load list of Investors
- When Investor is clicked load list of funds
- Ability to create and edit a new client. Client has name and description fields. -
Please feel free to make assumptions on any other key factors. Make sure to document your assumptions in the README.txt file in the project.


###Entities:

####Client
- id: Long
- name: String
- description: String
- investorList: List<Investor>

####Investor
- id: Long
- name: String
- description: String
- fundList: List<Fund>
- client: Client

####Fund
- id: Long
- name: String
- description: String
- totalFundAmount: BigDecimal
- investor: Investor

###Expectation for this task:
1. Sprint boot application with single module
2. Standard Java (and /or apache) libraries and collections are to be used.
3. Choice of JDK 1.8+
4. Rest API end point to build and retrieve the data
5. Simple JSON output is expected
6. Unit tests to test the key aspects of the logic is expected.
7. If you are using database, please feel free to use any DB of your choice or in-memory DB
8. Please use Angular 4 or later.
9. Good documentation across the application.
10. Make a build with maven or gradle
11. Check into git repo of your choice and send us the link once done
