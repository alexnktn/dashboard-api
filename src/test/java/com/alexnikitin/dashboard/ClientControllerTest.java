package com.alexnikitin.dashboard;

import com.alexnikitin.dashboard.config.H2TestJPAConfig;
import com.alexnikitin.dashboard.model.Client;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;
import java.util.List;

@RunWith(SpringRunner.class)
@ContextConfiguration(
        classes = { H2TestJPAConfig.class },
        loader = AnnotationConfigContextLoader.class)
public class ClientControllerTest {

    private static final String API_ROOT = "http://localhost:8080/dashboard/client";

    @Test
    public void whenGetAllClients_thenOK() {
        final Response response = RestAssured.get(API_ROOT);
        assertEquals(HttpStatus.OK.value(), response.getStatusCode());
    }

    @Test
    public void whenGetNotExistClientById_thenNotFound() {
        final Response response = RestAssured.get(API_ROOT + "/" + randomNumeric(4));
        assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatusCode());
    }

    // POST
    @Test
    public void whenCreateNewClient_thenCreated() {
        final Client client = createSampleClient();

        final Response response = RestAssured.given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(client)
                .post(API_ROOT);
        assertEquals(HttpStatus.CREATED.value(), response.getStatusCode());
    }

    @Test
    public void whenInvalidClient_thenError() {
        final Client client = createSampleClient();
        client.setName(null);

        final Response response = RestAssured.given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(client)
                .post(API_ROOT);
        assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatusCode());
    }

    @Test
    public void whenUpdateClient_thenUpdated() {
        final Client client = createSampleClient();
        final String location = createClientAsUri(client);

        client.setId(Long.parseLong(location.split("/client/")[1]));
        client.setName("Client New Name");
        Response response = RestAssured.given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(client)
                .put(location);
        assertEquals(HttpStatus.NO_CONTENT.value(), response.getStatusCode());

        response = RestAssured.get(location);
        assertEquals(HttpStatus.OK.value(), response.getStatusCode());
        assertEquals("Client New Name", response.jsonPath()
                .get("name"));

    }

    @Test
    public void whenDeleteCreatedClient_thenOk() {
        final Client client = createSampleClient();
        final String location = createClientAsUri(client);

        Response response = RestAssured.delete(location);
        assertEquals(HttpStatus.OK.value(), response.getStatusCode());

        response = RestAssured.get(location);
        assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatusCode());
    }

    /* Create mock data */

    private Client createSampleClient() {
        final Client client = new Client();
        client.setId(1);
        client.setName("Client name");
        client.setDescription("Mock client description");
        return client;
    }

    private String createClientAsUri(Client client) {
        final Response response = RestAssured.given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(client)
                .post(API_ROOT);
        return API_ROOT + "/" + response.jsonPath()
                .get("id");
    }
}
