package com.alexnikitin.dashboard.model;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class Fund {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column
    private String description;

    @Column
    private BigDecimal totalFundAmount;

    @ManyToOne
    @JoinColumn(name="investor_id")
    private Investor investor;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getTotalFundAmount() {
        return totalFundAmount;
    }

    public void setTotalFundAmount(BigDecimal totalFundAmount) {
        this.totalFundAmount = totalFundAmount;
    }

    public Investor getInvestor() {
        return investor;
    }

    public void setInvestor(Investor investor) {
        this.investor = investor;
    }
}
