package com.alexnikitin.dashboard.services;

import com.alexnikitin.dashboard.model.Investor;
import com.alexnikitin.dashboard.repositories.InvestorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Investor service that handles various investor operations such as creating, updating, deleting investor
 */
@Service
public class InvestorService {

    @Autowired
    private InvestorRepository investorRepository;

    /**
     * Update existed investor or create a new one
     * @param investor - Investor entity with detailed information
     */
    public void updateInvestor(Investor investor) {
        // TODO: validate edited entity
        investorRepository.save(investor);
    }

    /**
     * Create new investor. Calls updateInvestor(Investors) internally for persisting entity.
     * @see InvestorService#updateInvestor(Investor)
     */
    public void saveNewInvestor(Investor investor) {
        updateInvestor(investor);
    }

    /**
     * Delete existed investor
     * @param investor
     */
    public void deleteInvestor(Investor investor) {
        investorRepository.delete(investor);
    }
}
