package com.alexnikitin.dashboard.services;

import com.alexnikitin.dashboard.exception.EntityInvalidException;
import com.alexnikitin.dashboard.model.Client;
import com.alexnikitin.dashboard.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.persistence.EntityNotFoundException;

/**
 * Client service that handles various client operations such as creating, updating, deleting client
 */
@Service
public class ClientService {

    @Autowired
    private ClientRepository clientRepository;

    /**
     * Update existed client or create a new one
     * @param client - Client entity with detailed information
     */
    public Client updateClient(Client client) {
        clientRepository.findById(client.getId()).orElseThrow(EntityNotFoundException::new);
        if (isValidClient(client)) {
            return clientRepository.save(client);
        } else {
            throw new EntityInvalidException("Missing or invalid fields found for the client.");
        }
    }

    /**
     * Create new client. Calls updateClient(Clients) internally for persisting entity.
     * @see ClientService#updateClient(Client)
     */
    public Client saveNewClient(Client client) {
        if (isValidClient(client)) {
            return clientRepository.save(client);
        } else {
            throw new EntityInvalidException("Missing or invalid fields found for the client.");
        }
    }

    /**
     * Delete existed client
     * @param client
     */
    public void deleteClient(Client client) {
        clientRepository.delete(client);
    }

    /**
     * Validate client field prior to creating or editing. E.g. name, description, etc.
     *
     * @param client
     * @return True for valid client, false otherwise.
     */
    private boolean isValidClient(Client client) {
        if (StringUtils.isEmpty(client.getName()))
            return false;
        if (StringUtils.isEmpty(client.getDescription()))
            return false;
        return true;
    }

}
