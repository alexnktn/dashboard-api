package com.alexnikitin.dashboard.rest;

import com.alexnikitin.dashboard.exception.EntityIdMismatchException;
import com.alexnikitin.dashboard.model.Client;
import com.alexnikitin.dashboard.model.Investor;
import com.alexnikitin.dashboard.repositories.ClientRepository;
import com.alexnikitin.dashboard.repositories.InvestorRepository;
import com.alexnikitin.dashboard.services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

/**
 * Client rest controller
 */
@RestController
@RequestMapping(path="/client")
public class ClientController {

    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private InvestorRepository investorRepository;
    @Autowired
    private ClientService clientService;

    @RequestMapping(value = "/info", method = RequestMethod.GET, produces = "application/json")
    public String getInfo() {
        return "Running app: Dashboard 1.0.0";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Client> getClient(@PathVariable Long id) {
        Optional<Client> clientOptional = clientRepository.findById(id);
        if (clientOptional.isPresent()) {
            return new ResponseEntity<>(clientOptional.get(), HttpStatus.OK);
        } else {
            throw new EntityNotFoundException("Can't find client with ID " + id);
        }
    }

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List> getClients() {
        List<Client> clients = (List<Client>)clientRepository.findAll();
        return new ResponseEntity<>(clients, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity updateClient(@RequestBody Client client, @PathVariable Long id) {
        if (client.getId() != id) {
            throw new EntityIdMismatchException("Body id and URI id doesn't match");
        }
        clientService.updateClient(client);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity addClient(@RequestBody Client client) {
        Client saveClient = clientService.saveNewClient(client);
        return new ResponseEntity<>(saveClient, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}/investor", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List> getInvestorsForClient(@PathVariable Long id) {
        List<Investor> investors = investorRepository.findByClient_Id(id);
        return new ResponseEntity<>(investors, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteClient(@PathVariable Long id) {
        Optional<Client> clientOptional = clientRepository.findById(id);
        if (clientOptional.isPresent()) {
            clientService.deleteClient(clientOptional.get());
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Client not existed", HttpStatus.BAD_REQUEST);
        }
    }

}
