package com.alexnikitin.dashboard.rest;

import com.alexnikitin.dashboard.model.Fund;
import com.alexnikitin.dashboard.model.Investor;
import com.alexnikitin.dashboard.repositories.FundRepository;
import com.alexnikitin.dashboard.repositories.InvestorRepository;
import com.alexnikitin.dashboard.services.InvestorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * Investor rest controller
 */
@RestController
@RequestMapping(path="/investor")
public class InvestorController {

    @Autowired
    private InvestorService investorService;
    @Autowired
    private InvestorRepository investorRepository;
    @Autowired
    private FundRepository fundRepository;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Investor> findOne(@PathVariable Long id) {
        Optional<Investor> investorOptional = investorRepository.findById(id);
        if (investorOptional.isPresent()) {
            return new ResponseEntity<>(investorOptional.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List> getInvestors() {
        List<Investor> investors = (List<Investor>)investorRepository.findAll();
        return new ResponseEntity<>(investors, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity updateInvestor(@RequestBody Investor investor, @PathVariable Long id) {
        if (investor.getId() != id) {
            return new ResponseEntity("Body id and URI id doesn't match", HttpStatus.BAD_REQUEST);
        }
        investorService.updateInvestor(investor);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/{id}/fund", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List> getInvestorsForClient(@PathVariable Long id) {
        List<Fund> funds = fundRepository.findByInvestor_Id(id);
        return new ResponseEntity<>(funds, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity addInvestor(@RequestBody Investor investor) {
        investorService.saveNewInvestor(investor);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
