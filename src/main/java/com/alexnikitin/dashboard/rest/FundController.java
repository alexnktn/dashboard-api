package com.alexnikitin.dashboard.rest;

import com.alexnikitin.dashboard.model.Fund;
import com.alexnikitin.dashboard.model.Investor;
import com.alexnikitin.dashboard.repositories.FundRepository;
import com.alexnikitin.dashboard.repositories.InvestorRepository;
import com.alexnikitin.dashboard.services.InvestorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

/**
 * Fund rest controller
 */
@RestController
@RequestMapping(path="/fund")
public class FundController {

    @Autowired
    private FundRepository fundRepository;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Fund> findOne(@PathVariable Long id) {
        Optional<Fund> fundOptional = fundRepository.findById(id);
        if (fundOptional.isPresent()) {
            return new ResponseEntity<>(fundOptional.get(), HttpStatus.OK);
        } else {
            throw new EntityNotFoundException("Fund with id: " + id + " not found.");
        }
    }
}
