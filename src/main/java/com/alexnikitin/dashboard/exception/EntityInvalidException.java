package com.alexnikitin.dashboard.exception;

public class EntityInvalidException extends RuntimeException {
    public EntityInvalidException() {
        super();
    }

    public EntityInvalidException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public EntityInvalidException(final String message) {
        super(message);
    }

    public EntityInvalidException(final Throwable cause) {
        super(cause);
    }
}
