package com.alexnikitin.dashboard.repositories;

import com.alexnikitin.dashboard.model.Client;
import com.alexnikitin.dashboard.model.Investor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface InvestorRepository extends CrudRepository<Investor, Long> {
    List<Investor> findByClient_Id(Long id);
}
