package com.alexnikitin.dashboard.repositories;

import com.alexnikitin.dashboard.model.Client;
import org.springframework.data.repository.CrudRepository;

public interface ClientRepository extends CrudRepository<Client, Long> {
}
