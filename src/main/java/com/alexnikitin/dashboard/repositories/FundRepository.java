package com.alexnikitin.dashboard.repositories;

import com.alexnikitin.dashboard.model.Fund;
import com.alexnikitin.dashboard.model.Investor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface FundRepository extends CrudRepository<Fund, Long> {
    List<Fund> findByInvestor_Id(Long id);
}
