CREATE TABLE IF NOT EXISTS `client`
(
    `id` BIGINT(20) NOT NULL ,
    `name` VARCHAR(255) NOT NULL,
    `description` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `investor`
(
    `id` BIGINT(20) NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `description` VARCHAR(255) NOT NULL,
    `client_id` BIGINT(20) NOT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `FKma64kn4074b36hq83v5q7iifb` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
);

CREATE TABLE IF NOT EXISTS `fund`
(
    `id` BIGINT(20) NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `description` VARCHAR(255) NOT NULL,
    `total_fund_amount` DECIMAL (19,2),
    `investor_id` BIGINT(20) NOT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `FKm6uh3cqwerw6oue3ul7m748oq` FOREIGN KEY (`investor_id`) REFERENCES `investor` (`id`)
);

CREATE TABLE IF NOT EXISTS `hibernate_sequence`
(
  `next_val` BIGINT(20)
)
