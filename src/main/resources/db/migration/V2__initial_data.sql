INSERT INTO client (id, name, description)
VALUES
  (12, 'Bracebridge', 'Client number two'),
  (14, 'State Street', 'Boston location'),
  (15, 'BCG', 'Boston Consulting Group');

INSERT INTO investor (id, name, description, client_id)
VALUES
  (1, 'Fidelity', 'Mutual fund management', 12),
  (2, 'Cap', 'Private investment fund management', 12),
  (3, 'Alex Investment', '80% annual return guaranteed!', 14);

INSERT INTO fund (id, name, description, total_fund_amount, investor_id)
VALUES
  (1, 'FMEIX', 'Fidelity Mid-Cap Enhanced Index Fund', 440183.00, 1),
  (2, 'FLPSX', 'Fidelity Low-Priced Stock Fund', 1230141.00, 1),
  (3, 'FLVCX', 'Fidelity Leveraged Company Stock Fund', 230273.00, 1);
